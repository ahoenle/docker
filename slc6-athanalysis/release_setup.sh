#
# Environment configuration file for the ATLAS user. Can be sourced to set up
# the analysis release installed in the image.
#

# Set up a nice prompt:
export PS1='\[\033[01;35m\][bash]\[\033[01;31m\][\u AthAnalysis-$AthAnalysis_VERSION]\[\033[01;34m\]:\W >\[\033[00m\] ';

# Set up the compiler:
source /opt/lcg/gcc/6.2.0/x86_64-slc6/setup.sh
echo "Configured GCC from: /opt/lcg/gcc/6.2.0/x86_64-slc6"

# Set up (the rest of) LCG:
export LCG_RELEASE_BASE=/opt/lcg
echo "Taking LCG releases from: ${LCG_RELEASE_BASE}"

# Set up Gaudi:
GAUDI_VERSION=$(\cd /usr/GAUDI/;\ls)
GAUDI_PLATFORM=$(\cd /usr/GAUDI/${GAUDI_VERSION}/InstallArea/;\ls)
export GAUDI_ROOT=/usr/GAUDI/${GAUDI_VERSION}/InstallArea/${GAUDI_PLATFORM}
unset GAUDI_VERSION
unset GAUDI_PLATFORM
echo "Taking Gaudi from: ${GAUDI_ROOT}"

# Configure Calibration via HTTP
export PATHRESOLVER_ALLOWHTTPDOWNLOAD=1

# Set up the analysis release:
source /usr/AthAnalysis/*/InstallArea/*/setup.sh
echo "Configured AthAnalysis from: ${AthAnalysis_DIR}"
