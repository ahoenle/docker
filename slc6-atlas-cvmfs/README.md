SLC6 ATLAS OS
=============

This configuration can be used to set up a base image for ATLAS
Project builds

You can build it with:

```bash
docker build -t <username>/slc6-atlas-cvmfs:latest --build-arg user_id=$(id $(whoami) -u) --build-arg username=$(whoami) --build-arg group_id=$(id $(whoami) -g) --build-arg groupname=zp .
```

Compiler
--------

Note that the image holds GCC 6.2.0 out of the box. This is because **all**
production mode ATLAS releases use this compiler at the moment. Once this
changes, we will probably have to do something more elaborate here.
(For instance introducing an additional layer that just installs the right
compiler.)
