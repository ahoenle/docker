# SLC6 OS ATLAS nightly builds.

# Make the base image configurable:
ARG BASEIMAGE=cern/slc6-base:latest

# Set up the SLC6 "ATLAS OS":
FROM ${BASEIMAGE}

# Get user identity from host
ARG user_id
ARG group_id
ARG username
ARG groupname

# Perform the installation as root:
USER root
WORKDIR /root

# Put the repository configuration file(s) in place:
COPY *.repo /etc/yum.repos.d/
# Copy the prompt setup script in place:
COPY atlas_prompt.sh /etc/profile.d/

# 1. Install extra SLC6 and LCG packages needed by the ATLAS release
# 2. Install CMake
RUN yum -y install which git wget tar libxml2-devel atlas-devel \
      redhat-lsb-core libX11-devel libXpm-devel libXft-devel libXext-devel \
      openssl-devel glibc-devel rpm-build nano gcc_6.2.0_x86_64_slc6 sudo \
    && yum clean all \
    && wget https://cmake.org/files/v3.9/cmake-3.9.1-Linux-x86_64.tar.gz \
    && tar -C /usr/local --strip-components=1 --no-same-owner \
      -xvf cmake-3.9.1-Linux-x86_64.tar.gz \
    && rm cmake-3.9.1-Linux-x86_64.tar.gz

# Set up the user
RUN groupadd -g $group_id $groupname \
    && useradd -u $user_id -ms /bin/bash $username \
    && usermod -g $groupname $username

# Add basic usage instructions for the image:
COPY motd /etc/

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
