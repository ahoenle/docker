#
# Environment configuration file for the ATLAS user. Can be sourced to set up
# the athena release installed in the image.
#

# Set necessary variables to steer/complement AtlasSetup:
export SITEROOT=/usr
export TDAQ_RELEASE_BASE=/sw/atlas
export ATLAS_RELEASEDATA=/usr/atlas/offline/ReleaseData

# Set according to your location
export FRONTIER_SERVER='(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(serverurl=http://lcgft-atlas.gridpp.rl.ac.uk:3128/frontierATLAS)(serverurl=http://ccfrontier.in2p3.fr:23128/ccin2p3-AtlasFrontier)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy-wigner.cern.ch:3128)'

source /opt/atlas/AtlasSetup/scripts/asetup.sh Athena
